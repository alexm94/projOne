package com.revature.projectOneBeans;

public class UserRoles 
{
	private int roleID;
	private String role;
	
	public int getRoleID() {
		return roleID;
	}
	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return "UserRoles [roleID=" + roleID + ", role=" + role + "]";
	}
	
	public UserRoles(int roleID, String role) {
		super();
		this.roleID = roleID;
		this.role = role;
	}
}

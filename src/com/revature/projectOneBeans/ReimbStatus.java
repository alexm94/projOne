package com.revature.projectOneBeans;

public class ReimbStatus 
{
	private int statusID;
	private String reimbStatus;
	
	public int getStatusID() {
		return statusID;
	}
	public void setStatusID(int statusID) {
		this.statusID = statusID;
	}
	public String getReimbStatus() {
		return reimbStatus;
	}
	public void setReimbStatus(String reimbStatus) {
		this.reimbStatus = reimbStatus;
	}
	
	@Override
	public String toString() {
		return "ReimbStatus [statusID=" + statusID + ", reimbStatus=" + reimbStatus + "]";
	}
	
	public ReimbStatus(int statusID, String reimbStatus) {
		super();
		this.statusID = statusID;
		this.reimbStatus = reimbStatus;
	}
}

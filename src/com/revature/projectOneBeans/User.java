package com.revature.projectOneBeans;

public class User
{
	private int userID;
	private  String username;
	private String password;
	private String fName;
	private String lName;
	private String email;
	private int userRoleID;
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getUserRoleID() {
		return userRoleID;
	}
	public void setUserRoleID(int userRoleID) {
		this.userRoleID = userRoleID;
	}
	@Override
	public String toString() {
		return "User [userID=" + userID + ", username=" + username + ", password=" + password + ", fName=" + fName
				+ ", lName=" + lName + ", email=" + email + ", userRoleID=" + userRoleID + "]";
	}
	
	public User(int userID, String username, String password, String fName, String lName, String email,
			int userRoleID) {
		super();
		this.userID = userID;
		this.username = username;
		this.password = password;
		this.fName = fName;
		this.lName = lName;
		this.email = email;
		this.userRoleID = userRoleID;
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}
}

package com.revature.projectOneBeans;

public class ReimbType 
{
	private int typeID;
	private String type;
	
	public int getTypeID() {
		return typeID;
	}
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "ReimbType [typeID=" + typeID + "]";
	}
	
	public ReimbType(int typeID, String type) {
		super();
		this.typeID = typeID;
		this.type = type;
	}
}

package com.revature.projectOneDAO;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.revature.projectOneBeans.Reimbursement;
import com.revature.projectOneBeans.User;

import oracle.sql.DATE;

public class ReimbDAO 
{
	//jdbc:oracle:thin:@localhost:1521:ers_reimbursement:xe
	static String tns = "jdbc:oracle:thin:@localhost:1521:xe";	//TNS name = driver:driver:driver@location:port:SID
	static String user = "ersProject";
	static String pass = "ersproject";
	private static java.sql.Connection conn;
	Date date;
	
	private static Properties reimbColumns;
	
//	static
	{
		reimbColumns = new Properties();
		
		InputStream in = getClass().getResourceAsStream("reimb.properties");
		
		try 
		{
			reimbColumns.load(in);
			
			in.close();
		} 
		
		catch (IOException e) 
		{
			System.out.println("Couldn't load props.");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*try 
		{
			reimbColumns.load(
					ReimbDAO.class.getClassLoader()
					.getResourceAsStream("reimb.properties"));
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}*/
	}
	
	public ReimbDAO()
	{
		
		try 
		{
//			new com.revature.service.ServiceLoader();
//			ServiceLoader.getDemoDB();
			conn = DriverManager.getConnection(tns, user, pass);  //Start connection
			conn.setAutoCommit(false);  // enable transactions
		}
		
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		//ACID
		//A -- Atomic, all or nothing updates
		//C -- consistent, fail rollback success commit
		//I -- isolation, no concurrency issuse
		//D -- durable, survive physical problems
		

	}
	
	@Override
	protected void finalize() throws Throwable
	{
		if(!conn.isClosed()) conn.close();
	}
	
	public boolean validateUser(String user, String pass)
	{
		
		ResultSet rs = null;

		boolean flag = true;
		
		try 
		{
			String query = "select "
						 + reimbColumns.getProperty("userID")
						 + " from ERS_USERS "
						 + " where " + reimbColumns.getProperty("uName") + " = ? and "
						 + reimbColumns.getProperty("uPass") + " = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setString(1, user);
			pstmt.setString(2, pass);
			
			rs = pstmt.executeQuery();

			if(rs.next()) 
			{
				System.out.println(rs.getInt(1));
				flag = true;
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			flag = false;
		}
		
		return flag;
	}
	
	public int getUserID(String user)
	{
		int userID = -1;
		ResultSet rs = null;

		try 
		{
			String testQ = "SELECT ers_users_id FROM ers_users WHERE ers_user_name = 'manager'";
			
			String query = "select "
				     + "ers_users_id"
					 + " from ers_users"
					 + " where ers_user_name = '" + user +"'";
			
			//System.out.println("UserID: " + reimbColumns.getProperty("userID"));
		
//			Statement stmt = conn.createStatement();
//			rs = stmt.executeQuery(testQ);
			
			PreparedStatement pstmt;
			pstmt = conn.prepareStatement(query);
			
//			pstmt.setString(1, user);
//			conn.commit();
			
			System.out.println("UserID: " + userID);
			
			rs = pstmt.executeQuery(query);
			//(;//)executeQuery();
			if(rs.next())userID = rs.getInt(1);
			
			System.out.println("UserID: " + userID);
		} 
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			System.out.println("validateError.");
			e.printStackTrace();
		}
		
		return userID;
	}
	
	public int getUserRoleID(String user)
	{
		int userRoleID = -1;
		ResultSet rs = null;

		try 
		{
			String testQ = "SELECT ers_users_id FROM ers_users WHERE ers_user_name = 'manager'";
			
			String query = "select "
				     + reimbColumns.getProperty("uRoleID")
					 + " from ers_users"
					 + " where " + reimbColumns.getProperty("uName") + " = '" + user +"'";
			
			//System.out.println("UserID: " + reimbColumns.getProperty("userID"));
		
//			Statement stmt = conn.createStatement();
//			rs = stmt.executeQuery(testQ);
			
			PreparedStatement pstmt;
			pstmt = conn.prepareStatement(query);
			
//			pstmt.setString(1, user);
//			conn.commit();
			
			System.out.println("UserRoleID: " + userRoleID);
			
			rs = pstmt.executeQuery(query);
			//(;//)executeQuery();
			if(rs.next())userRoleID = rs.getInt(1);
			
			System.out.println("UserRoleID: " + userRoleID);
		} 
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			System.out.println("validateError.");
			e.printStackTrace();
		}
		
		return userRoleID;
	}
	
	public int requestReimb(Reimbursement obj)
	{
		int retInt = 0;
		System.out.println("Requesting reimb.");
		
		try
		{
			//SQL stmt
			String sql = "insert into ers_reimbursement("
					   + reimbColumns.getProperty("reimbID") + ", "
					   + reimbColumns.getProperty("amnt") + ", "
					   + reimbColumns.getProperty("submitted") + ", "
					   + reimbColumns.getProperty("resolved") + ", "
					   + reimbColumns.getProperty("description") + ", "
					   + reimbColumns.getProperty("receipt") + ", "
					   + reimbColumns.getProperty("author") + ", "
					   + reimbColumns.getProperty("agent") + ", "
					   + reimbColumns.getProperty("statusID") + ", "
					   + reimbColumns.getProperty("typeID")
					   + " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			System.out.println("Query: " + sql);
			
			PreparedStatement pstmt = conn.prepareStatement(sql);	//compiled by java
			pstmt.setInt(1, obj.getReimbID());
			pstmt.setDouble(2,  obj.getReimbAmount());
			pstmt.setDate(3, null);
			pstmt.setDate(4, null);
			pstmt.setString(5, obj.getReimbDescription());
			pstmt.setBlob(6, obj.getReimbReceipt());
			pstmt.setInt(6, obj.getReimbAuthor());
			pstmt.setInt(6, obj.getReimbAgent());
			pstmt.setInt(6, obj.getReimbStatusID());
			pstmt.setInt(6, obj.getReimbTypeID());
			conn.commit();
			
			System.out.println("Commited!");
		}
		
		catch(Throwable e)
		{
			try 
			{
				System.out.println("Reimb submit didn't work.");
				retInt = -1;
				conn.rollback();
			} 
			
			catch (SQLException e1) 
			{
				retInt = -1;
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		return retInt;
	}
	
	public ArrayList<Reimbursement> getAllReimbs()
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
					     + reimbColumns.getProperty("reimbID") + ", "
					     + reimbColumns.getProperty("amnt") + ", "
					     + reimbColumns.getProperty("submitted") + ", "
					     + reimbColumns.getProperty("resolved") + ", "
					     + reimbColumns.getProperty("description") + ", "
					     + reimbColumns.getProperty("receipt") + ", "
					     + reimbColumns.getProperty("author") + ", "
					     + reimbColumns.getProperty("agent") + ", "
					     + reimbColumns.getProperty("statusID") + ", "
					     + reimbColumns.getProperty("typeID")
						 + " from ers_reimbursement";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("reimbID")),
						rs.getDouble(reimbColumns.getProperty("amnt")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("statusID")),
						rs.getInt(reimbColumns.getProperty("typeID"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public ArrayList<Reimbursement> getAllReimbs(int userID)
	{
		System.out.println("\nIn AllReimbs.");
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			System.out.println("Trying to execute query.");
			
			System.out.println(conn);
			
			String tQuery = "select * from ers_reimbursement where reimb_author = 321";
			
			String query = "select *"
			     /*+ reimbColumns.getProperty("reimbID") + ", "
			     + reimbColumns.getProperty("amnt") + ", "
			     + reimbColumns.getProperty("submitted") + ", "
			     + reimbColumns.getProperty("resolved") + ", "
			     + reimbColumns.getProperty("description") + ", "
			     + reimbColumns.getProperty("receipt") + ", "
			     + reimbColumns.getProperty("author") + ", "
			     + reimbColumns.getProperty("agent") + ", "
			     + reimbColumns.getProperty("statusID") + ", "
			     + reimbColumns.getProperty("typeID")*/
				 + " from ers_reimbursement where "
				 + reimbColumns.getProperty("author") + " = 321";
				 //+ "ers_users." + reimbColumns.getProperty("userID") + " = ?";

			System.out.println("Query: " + tQuery);
			
			PreparedStatement pstmt = conn.prepareStatement(tQuery);
			/*pstmt.setInt(1, 321);*/
			
			System.out.println("UserID: " + userID);
			
			rs = pstmt.executeQuery();
			
			System.out.println("Executed.");
			
			while(rs.next())
			{
				System.out.println("In while loop.");
				
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("reimbID")),
						rs.getDouble(reimbColumns.getProperty("amnt")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("author")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("statusID")),
						rs.getInt(reimbColumns.getProperty("typeID"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public ArrayList<Reimbursement> getPendingReimbs()
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
					     + reimbColumns.getProperty("reimbID") + ", "
					     + reimbColumns.getProperty("amnt") + ", "
					     + reimbColumns.getProperty("submitted") + ", "
					     + reimbColumns.getProperty("resolved") + ", "
					     + reimbColumns.getProperty("description") + ", "
					     + reimbColumns.getProperty("receipt") + ", "
					     + reimbColumns.getProperty("author") + ", "
					     + reimbColumns.getProperty("agent") + ", "
					     + reimbColumns.getProperty("statusID") + ", "
					     + reimbColumns.getProperty("typeID")
					     + " from ers_reimbursement where "
					     + reimbColumns.getProperty("statusID") + " = 0";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("status")),
						rs.getInt(reimbColumns.getProperty("type"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public ArrayList<Reimbursement> getPendingReimbs(int uID)
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
				     + reimbColumns.getProperty("reimbID") + ", "
				     + reimbColumns.getProperty("amnt") + ", "
				     + reimbColumns.getProperty("submitted") + ", "
				     + reimbColumns.getProperty("resolved") + ", "
				     + reimbColumns.getProperty("description") + ", "
				     + reimbColumns.getProperty("receipt") + ", "
				     + reimbColumns.getProperty("author") + ", "
				     + reimbColumns.getProperty("agent") + ", "
				     + reimbColumns.getProperty("statusID") + ", "
				     + reimbColumns.getProperty("typeID")
				     + " from ers_reimbursement where "
				     + reimbColumns.getProperty("statusID") + " = 0"
				     + " and " + reimbColumns.getProperty("userID") + " = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, uID);
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("reimbID")),
						rs.getDouble(reimbColumns.getProperty("amnt")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("statusID")),
						rs.getInt(reimbColumns.getProperty("typeID"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public ArrayList<Reimbursement> getApprovedReimbs()
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
				     + reimbColumns.getProperty("reimbID") + ", "
				     + reimbColumns.getProperty("amnt") + ", "
				     + reimbColumns.getProperty("submitted") + ", "
				     + reimbColumns.getProperty("resolved") + ", "
				     + reimbColumns.getProperty("description") + ", "
				     + reimbColumns.getProperty("receipt") + ", "
				     + reimbColumns.getProperty("author") + ", "
				     + reimbColumns.getProperty("agent") + ", "
				     + reimbColumns.getProperty("statusID") + ", "
				     + reimbColumns.getProperty("typeID")
				     + " from ers_reimbursement where "
				     + reimbColumns.getProperty("statusID") + " = 1";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("reimbID")),
						rs.getDouble(reimbColumns.getProperty("amnt")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("statusID")),
						rs.getInt(reimbColumns.getProperty("typeID"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public ArrayList<Reimbursement> getApprovedReimbs(int uID)
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
				     + reimbColumns.getProperty("reimbID") + ", "
				     + reimbColumns.getProperty("amnt") + ", "
				     + reimbColumns.getProperty("submitted") + ", "
				     + reimbColumns.getProperty("resolved") + ", "
				     + reimbColumns.getProperty("description") + ", "
				     + reimbColumns.getProperty("receipt") + ", "
				     + reimbColumns.getProperty("author") + ", "
				     + reimbColumns.getProperty("agent") + ", "
				     + reimbColumns.getProperty("statusID") + ", "
				     + reimbColumns.getProperty("typeID")
				     + " from ers_reimbursement where "
				     + reimbColumns.getProperty("statusID") + " = 1"
				     + " and " + reimbColumns.getProperty("userID") + " = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, uID);
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("status")),
						rs.getInt(reimbColumns.getProperty("type"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public ArrayList<Reimbursement> getDeniedReimbs()
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
				     + reimbColumns.getProperty("reimbID") + ", "
				     + reimbColumns.getProperty("amnt") + ", "
				     + reimbColumns.getProperty("submitted") + ", "
				     + reimbColumns.getProperty("resolved") + ", "
				     + reimbColumns.getProperty("description") + ", "
				     + reimbColumns.getProperty("receipt") + ", "
				     + reimbColumns.getProperty("author") + ", "
				     + reimbColumns.getProperty("agent") + ", "
				     + reimbColumns.getProperty("statusID") + ", "
				     + reimbColumns.getProperty("typeID")
				     + " from ers_reimbursement where "
				     + reimbColumns.getProperty("statusID") + " = 2";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("status")),
						rs.getInt(reimbColumns.getProperty("type"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public ArrayList<Reimbursement> getDeniedReimbs(int uID)
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
				     + reimbColumns.getProperty("reimbID") + ", "
				     + reimbColumns.getProperty("amnt") + ", "
				     + reimbColumns.getProperty("submitted") + ", "
				     + reimbColumns.getProperty("resolved") + ", "
				     + reimbColumns.getProperty("description") + ", "
				     + reimbColumns.getProperty("receipt") + ", "
				     + reimbColumns.getProperty("author") + ", "
				     + reimbColumns.getProperty("agent") + ", "
				     + reimbColumns.getProperty("statusID") + ", "
				     + reimbColumns.getProperty("typeID")
				     + " from ers_reimbursement where "
				     + reimbColumns.getProperty("statusID") + " = 2"
				     + " and " + reimbColumns.getProperty("userID") + " = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, uID);
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("status")),
						rs.getInt(reimbColumns.getProperty("type"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public ArrayList<Reimbursement> getResolvedReimbs()
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
				     + reimbColumns.getProperty("reimbID") + ", "
				     + reimbColumns.getProperty("amnt") + ", "
				     + reimbColumns.getProperty("submitted") + ", "
				     + reimbColumns.getProperty("resolved") + ", "
				     + reimbColumns.getProperty("description") + ", "
				     + reimbColumns.getProperty("receipt") + ", "
				     + reimbColumns.getProperty("author") + ", "
				     + reimbColumns.getProperty("agent") + ", "
				     + reimbColumns.getProperty("statusID") + ", "
				     + reimbColumns.getProperty("typeID")
				     + " from ers_reimbursement where "
				     + reimbColumns.getProperty("statusID") + " > 0";


			PreparedStatement pstmt = conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("status")),
						rs.getInt(reimbColumns.getProperty("type"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	public ArrayList<Reimbursement> getResolvedReimbs(int uID)
	{
		ResultSet rs = null;
		ArrayList<Reimbursement> results = new ArrayList<Reimbursement>();
		
		try 
		{
			String query = "select "
				     + reimbColumns.getProperty("reimbID") + ", "
				     + reimbColumns.getProperty("amnt") + ", "
				     + reimbColumns.getProperty("submitted") + ", "
				     + reimbColumns.getProperty("resolved") + ", "
				     + reimbColumns.getProperty("description") + ", "
				     + reimbColumns.getProperty("receipt") + ", "
				     + reimbColumns.getProperty("author") + ", "
				     + reimbColumns.getProperty("agent") + ", "
				     + reimbColumns.getProperty("statusID") + ", "
				     + reimbColumns.getProperty("typeID")
				     + " from ers_reimbursement where "
				     + reimbColumns.getProperty("statusID") + " > 0"
				     + " and " + reimbColumns.getProperty("userID") + " = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, uID);
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Reimbursement obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("status")),
						rs.getInt(reimbColumns.getProperty("type"))
						);
				
				results.add(obj);
			}
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}

	/*public Reimbursement withdrawReimb(int reimbID)
	{
		Reimbursement obj = null;
		
		try 
		{
			String query = "update ers_reimbursement"
						 + "set reimb_type_id = ?"
						 + " where type = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, 1);
			pstmt.setInt(2, reimbID);

			ResultSet rs;
			
			rs = pstmt.executeQuery();
			
			while(rs.next()) //for-each row
			{
				obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("submitted")),
						rs.getDate(reimbColumns.getProperty("resolved")),
						rs.getString(reimbColumns.getProperty("description")),
						rs.getBlob(reimbColumns.getProperty("receipt")),
						rs.getInt(reimbColumns.getProperty("agent")),
						rs.getInt(reimbColumns.getProperty("resolver")),
						rs.getInt(reimbColumns.getProperty("status")),
						rs.getInt(reimbColumns.getProperty("type"))
						);

				if(obj.getReimbID() == reimbID)	
				{
					System.out.println("Reimbursement updated!");
					break;
				}
			}
		} 
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			obj = null;
			e.printStackTrace();
		}
		
		return obj;
	}
	*/
	
	public void approveReimb(int reimbID, int agentID)
	{
		try 
		{
			String query = "update ers_reimbursement"
						 + "set "
						 + reimbColumns.getProperty("typeID") + " = ?, "
						 + reimbColumns.getProperty("agent") + " = ?, "
						 + reimbColumns.getProperty("resolved") + " = sysdate, "
						 + " where "
						 + reimbColumns.getProperty("reimbID") + " = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, 1);
			pstmt.setInt(2, agentID);
			pstmt.setInt(3, reimbID);

			/*ResultSet rs;*/
			
			/*rs =*/ pstmt.executeQuery();
		} 
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void denyReimb(int reimbID, int agentID)
	{		
		try 
		{
			String query = "update ers_reimbursement"
					 + "set "
					 + reimbColumns.getProperty("typeID") + " = ?, "
					 + reimbColumns.getProperty("agent") + " = ?, "
					 + reimbColumns.getProperty("resolved") + " = sysdate, "
					 + " where "
					 + reimbColumns.getProperty("reimbID") + " = ?";

			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, 2);
			pstmt.setInt(2, agentID);
			pstmt.setInt(3, reimbID);


			/*ResultSet rs;*/
			
			/*rs =*/ pstmt.executeQuery();
		} 
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
			
	public int getHighestReimbID()
	{
		int highest = 0;
		
		System.out.println("Getting highest.");
		
		try 
		{
			String query = "select max("
						 + reimbColumns.getProperty("reimbID") + ")"
						 + " from ers_reimbursement";
			
			ResultSet rs;

			PreparedStatement pstmt = conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			System.out.println("Executed highest query.");
			
			if(rs.next()) highest = (int) rs.getInt(1);
			
			System.out.println(highest);
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			highest = 0;
			e.printStackTrace();
		}
		
		return highest;
	}
	
	public void createUser(int uID, String uName, String uPass, String fName, String lName, String email, int roleID)
	{
		try 
		{
			String query = "insert into ers_users("
						 + reimbColumns.getProperty("userID") + ", "
						 + reimbColumns.getProperty("uName") + ", "
						 + reimbColumns.getProperty("uPass") + ", "
						 + reimbColumns.getProperty("fName") + ", "
						 + reimbColumns.getProperty("lName") + ", "
						 + reimbColumns.getProperty("uEmail") + ", "
						 + reimbColumns.getProperty("uRoleID") + ") "
						 + "values(?, ?, ?, ?, ?, ?, ?)";
			
			PreparedStatement pstmt = conn.prepareStatement(query);
			
			pstmt.setInt(1, uID);
			pstmt.setString(2, uName);
			pstmt.setString(3, md5(uPass));
			pstmt.setString(4, fName);
			pstmt.setString(5, lName);
			pstmt.setString(6, email);
			pstmt.setInt(7, roleID);
			
			/*ResultSet rs =*/ pstmt.executeQuery();
			
			conn.commit();
		} 
			
		catch (SQLException e) 
		{
			try 
			{
				conn.rollback();
			} 
			
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
		
	public Reimbursement deleteReimb(int id)
	{
		Reimbursement obj = null;
		
		try 
		{
			String query = "delete from ers_reimbursement"
						 + "where id = ?";
			ResultSet rs;

			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()) //for-each row
			{
				obj = new Reimbursement(
						rs.getInt(reimbColumns.getProperty("id")),
						rs.getDouble(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("amount")),
						rs.getDate(reimbColumns.getProperty("amount")),
						rs.getString(reimbColumns.getProperty("amount")),
						rs.getBlob(reimbColumns.getProperty("amount")),
						rs.getInt(reimbColumns.getProperty("amount")),
						rs.getInt(reimbColumns.getProperty("amount")),
						rs.getInt(reimbColumns.getProperty("amount")),
						rs.getInt(reimbColumns.getProperty("amount"))
						);

				if(obj.getReimbID() == id)	
				{
					System.out.println("Reimbursement deleted!");
					break;
				}
			}
		}
	
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			obj = null;
			e.printStackTrace();
		}
		
		return obj;
	}

	public static String md5(String input) {
		
		String md5 = null;
		
		if(null == input) return null;
		
		try {
			
		//Create MessageDigest object for MD5
		MessageDigest digest = MessageDigest.getInstance("MD5");
		
		//Update input string in message digest
		digest.update(input.getBytes(), 0, input.length());

		//Converts message digest value in base 16 (hex) 
		md5 = new BigInteger(1, digest.digest()).toString(16);

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return md5;
	}
}























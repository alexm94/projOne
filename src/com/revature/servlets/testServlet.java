package com.revature.servlets;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.*;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Properties;

import javax.persistence.criteria.CriteriaBuilder.In;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.projectOneBeans.Reimbursement;
import com.revature.projectOneBeans.User;
import com.revature.projectOneDAO.ReimbDAO;
import com.revature.projectOneService.ServiceLocator;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleDriver;

@SuppressWarnings("serial")
public class testServlet extends HttpServlet
{
	//jdbc:oracle:thin:@localhost:1521:ers_reimbursement:xe
	static String tns = "jdbc:oracle:thin:@localhost:1521:xe";	//TNS name = driver:driver:driver@location:port:SID
	static String user = "ersProject";
	static String pass = "ersproject";
	private static java.sql.Connection conn;
	
	Properties prop = new Properties();
	
	@Override
	public void init() throws ServletException 
	{
		System.out.println("In init().");

		try 
		{
			System.out.println("Trying the fucking conn.");

			prop.setProperty("user", user);
			prop.setProperty("password", pass);
			
			OracleDriver dr = new OracleDriver();
			
			OracleConnection oConn = (OracleConnection) dr.connect(tns, prop);
			
			/** DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
			
			conn = DriverManager.getConnection("tns", "user", "pass");  //Start connection
			conn.setAutoCommit(false);  // enable transactions	
			**/
			
			System.out.println("Got the fucking conn.");
		} 
		
		catch (SQLException e) 
		{
			System.out.println("Fuck. Again.");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		/*new ServiceLocator();
		ServiceLocator.getDB();*/
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		this.doPost(request, response);
	}

	@SuppressWarnings("null")
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		User comrade = null;
		HttpSession session = null;
		
		session = request.getSession();
		System.out.println("in post().");
		
		switch(request.getRequestURI())
		{
			case "/cssTesting/cu.do":
			{
				System.out.println("Hi");
				
				session = request.getSession();
				new ReimbDAO();
				
				System.out.println(request.getParameter("userID"));
				
				new ReimbDAO().createUser(
						Integer.parseInt(request.getParameter("userID")),
						request.getParameter("uname"),
						ReimbDAO.md5(request.getParameter("upass")),
						request.getParameter("fname"),
						request.getParameter("lname"),
						request.getParameter("email"),
						Integer.parseInt(request.getParameter("urid"))
						);

				request.setAttribute("loginFail", "User created!");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}break;
		
			case "/cssTesting/login.do":
			{
				boolean flag = false;
				
				/*if(request.getParameter("username").equals("danny") && request.getParameter("password").equals("boy"))
				{
					System.out.println("Going home.");
					session = request.getSession();
					session.setAttribute("uID", "danny");
					session.setAttribute("uRoleID", 0);
					request.getRequestDispatcher("/secure/home.jsp").forward(request, response);
					System.out.println("Got home");
				}*///Test bs ignore
				
				new ReimbDAO();
				String uName = request.getParameter("username");
				String uPass = ReimbDAO.md5(request.getParameter("password"));
				flag = new ReimbDAO().validateUser(uName, uPass);
				
				if(flag == true)
				{
					session.setAttribute("uID", new ReimbDAO().getUserID(uName));
					session.setAttribute("uRoleID", new ReimbDAO().getUserRoleID(uName));
					response.sendRedirect("./secure/home.jsp");
				}
				
				else
				{
					session.setAttribute("loginFail", "Login failed. Try again.");
					response.sendRedirect("login.jsp");
				}
			}break;
			
			case "/cssTesting/secure/reimbNewSub.do":
			{
				if(session.getAttribute("uID") != null)
				{	
			        Blob imgBlob = null;
			        
					if(request.getParameter("filePath") != null)
					{
						System.out.println("File path: " + request.getParameter("filePath"));
						File image = new File((String) request.getParameter("filePath"));
						InputStream stream = new FileInputStream(image);
						
				        ByteArrayOutputStream bos = new ByteArrayOutputStream();
				        byte[] buf = new byte[1024];
				        try {
				            for (int readNum; (readNum = stream.read(buf)) != -1;) {
				                bos.write(buf, 0, readNum); //no doubt here is 0
				                //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
				                System.out.println("read " + readNum + " bytes,");
				            }
				        } catch (IOException ex) 
				        {
				        	System.out.println("Fuck.");
				        }
				        byte[] bytes = bos.toByteArray();
				        
				        Connection conn;
//				        imgBlob = null;
				        
						try 
						{
							conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:ers_reimbursement:xe", "ersProject", "ersProject");
					        imgBlob = conn.createBlob();
					        imgBlob.setBytes(0, bytes);
						} 
						
						catch (SQLException e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
			        Date now = new Date(0);
			        
			        int type = 0;
			        
			        switch ((String) request.getParameter("type")) 
			        {
						case "Food": type = 0;
							break;
						case "Travel": type = 1;
							break;
						case "Lodging": type = 2;
							break;
						case "Other": type = 3;
							break;
						default:break;
					}

			        System.out.println("Amount: " + Double.parseDouble(request.getParameter("amount")));
			        System.out.println("Description: " + request.getParameter("descript"));
			        System.out.println("Type: " + request.getParameter("type"));
					
					Reimbursement reimb = new Reimbursement(
							new ReimbDAO().getHighestReimbID()+1, 
							Double.parseDouble(request.getParameter("amount")), 
							now,
							null, 
							request.getParameter("description"), 
							imgBlob, 
							(int) session.getAttribute("uID"), 
							0,
							0, 
							type
							);
					
					new ReimbDAO().requestReimb(reimb);
					
					response.sendRedirect("./reimbList.jsp");	
				}
			}break;
			
			case "/cssTesting/secure/reimbsAll.do":
			{
				if(session.getAttribute("uID") != null)
				{				
					System.out.println("Inside reimbsAll.");
					switch((int) session.getAttribute("uRoleID") /*comrade.getUserRoleID()*/)
					{
						case 0:  //case Manager
						{
							System.out.println("In case 0.");
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getAllReimbs();
							session.setAttribute("reimbs", reimbs);
//							request.getRequestDispatcher("./cssTesting/WebContent/secure/reimbList.jsp").forward(request, response);
							response.sendRedirect("./reimbList.jsp");
						}break;
						
						case 1:  //case Employee
						{
							System.out.println("In case 1.");
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getAllReimbs((int) session.getAttribute("uID"));
							
							System.out.println("reimbs size: " + reimbs.size());
							
							session.setAttribute("reimbs", reimbs);
							response.sendRedirect("./reimbList.jsp");						
						}break;
						
						default: response.sendRedirect("errorPage.jsp");
					}
				}
				
				else response.sendRedirect("login.jsp");
			}break;
			
			case "/cssTesting/secure/reimbApp.do":
			{
				if(session.getAttribute("uID") != null)
				{	
					switch(comrade.getUserRoleID())
					{
						case 0:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getApprovedReimbs();
							session.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");
						}break;
						
						case 1:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getApprovedReimbs((int) session.getAttribute("uID"));
							session.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");						
						}break;
						
						default: response.sendRedirect("errorPage.jsp");
					}
				}
				
				else response.sendRedirect("login.jsp");
			}break;
			
			case "/cssTesting/secure/reimbDen.do":
			{
				if(request.getAttribute("uID") != null)
				{	
					switch(comrade.getUserRoleID())
					{
						case 0:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getDeniedReimbs();
							request.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");
						}break;
						
						case 1:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getDeniedReimbs((int) session.getAttribute("uID"));
							request.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");						
						}break;
						
						default: response.sendRedirect("errorPage.jsp");
					}
				}
					
				else response.sendRedirect("/secure/login.jsp");
			}break;
			
			case "/cssTesting/secure/reimbPen.do":
			{
				if(request.getAttribute("uID") != null)
				{	
					switch(comrade.getUserRoleID())
					{
						case 0:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getPendingReimbs();
							request.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");
						}break;
						
						case 1:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getPendingReimbs((int) session.getAttribute("uID"));
							request.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");						
						}break;
						
						default: response.sendRedirect("errorPage.jsp");
					}
				}
					
				else response.sendRedirect("login.jsp");
			}break;
			
			case "/cssTesting/secure/reimbRes.do":
			{
				if(request.getAttribute("uID") != null)
				{	
					switch(comrade.getUserRoleID())
					{
						case 0:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getResolvedReimbs();
							request.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");
						}break;
						
						case 1:
						{
							ArrayList<Reimbursement> reimbs = new ReimbDAO().getResolvedReimbs((int) session.getAttribute("uID"));
							request.setAttribute("reimbs", reimbs);
							response.sendRedirect("/secure/reimbList.jsp");						
						}break;
						
						default: response.sendRedirect("errorPage.jsp");
					}
				}
					
				else response.sendRedirect("login.jsp");
			}break;
			
			case "/cssTesting/secure/approve.do":
			{
				if(request.getAttribute("uID") != null)
				{
					int agent = comrade.getUserID();
					
					new ReimbDAO().approveReimb((int) request.getAttribute("reimbID"), agent);
				}
					
				else response.sendRedirect("login.jsp");
			}break;
			
			case "/cssTesting/secure/deny.do":
			{
				if(request.getAttribute("uID") != null)
				{
					int agent = comrade.getUserID();
					
					new ReimbDAO().denyReimb((int) request.getAttribute("reimbID"), agent);
				}
					
				else response.sendRedirect("login.jsp");
			}break;

			case "/cssTesting/secure/testHelp.jsp":
			{
				response.sendRedirect("/secure/testHelp.jsp");
			}break;
			
			case "/cssTesting/secure/logout.do":
			{
				request.getSession().invalidate();
				response.sendRedirect("../login.jsp");
			}break;
			
			default: 
				{
					System.out.println("Shitshitshit!");
					response.sendRedirect("errorPage.jsp");
				}
		}
		
		
	}
	
}

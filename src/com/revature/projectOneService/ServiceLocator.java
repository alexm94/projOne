package com.revature.projectOneService;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.revature.projectOneDAO.ReimbDAO;

public class ServiceLocator 
{
	public static DataSource getDB()
	{
		System.out.println("In getDB().");
		try 
		{
			System.out.println("Loading props.");
			
			Properties environmentProps = new Properties();
			environmentProps.load(
					ReimbDAO.class.getClassLoader().getResourceAsStream("jndi.properties"));
			
			System.out.println("Props loaded.");

			Context ctxt = new InitialContext(environmentProps);
			DataSource ds = (DataSource) ctxt.lookup("db/chinook");
			
			System.out.println("Database found.");
			
			return ds;
		} 
	
		catch (Exception e) 
		{
			System.out.println("FUCK!");
			
			return null;
		}
	}

}

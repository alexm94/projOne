<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>

<link rel="stylesheet" href="overridesTest.css">
</head>
<body>
	<ul class="navList">
	  <li><a class="active" href="#home">Home</a></li>
	  <li><a href="#">Account</a></li>
	  <li class="dropdown">
	    <a href="#" class="dropbtn">Help</a>
	    <div class="dropdown-content">
	      <form action="help.do" method="post"><a type="submit">FAQs</a></form>
	      <a href="#">Contact someone!</a>
	    </div>
	  </li>
	  <li id="comrade">Hello Comrade  <c:out value=${"loggedIn"}/>!</li>
	  <li><a>Logout</a></li>
	</ul>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type="text/javascript">

function radio()
{
	var radioBtns=document.getElementsByName("reimbType");
	for(i=0;i<radioBtns.length; i++)
	{
	    if(radioBtns[i].checked)
	    {
	    	switch(i)
	    	{
	    		case 0: <c:set var="type" value="Food" scope="request"/>
	    		break;
	    		case 1: <c:set var="type" value="Travel" scope="request"/>
	    		break;
	    		case 2: <c:set var="type" value="Lodging" scope="request"/>
	    		break;
	    		case 3: <c:set var="type" value="Other" scope="request"/>
	    		break;
	    	}
	    	break;
	        alert(radioBtns[i].value);
	    }
	}
}

function getPath()
{
	var path1 = document.getElementById("upload").value;
	var text = document.getElementById("path");
	text.value = path1;
}
	
</script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <script>
    function openReimb() 
    {
      window.open("./reimbDetails.jsp", null, "height=600, width=400, status=yes, toolbar=no, menubar=no, location=no");
    }
    
    function unhide(divID) 
    {
	    var item = document.getElementById(divID);
	    if (item) 
	    {
	      item.className=(item.className=='hidden')?'unhidden':'hidden';
	    }
	}
  </script>
  
</head>
<body>
	<%@ include file="./navTest.jsp"%>
	
	<div class="rcorners1">
		<h1 id="reimbListHead">We've been watching you, Comrade...</h1>
		<p id="reimbPad">Here, you will see your requested reimbursements, and the expenses we have deemed...<i>socially beneficial</i>.</p>
	</div>
	
	<div id="reimbColumns">
		<div id="reimbListBtns">
			<table class="btnTable">
				<tr>
					<th><form action="reimbAll.do" method="post"><input type="submit" class="btnSmall" value="All Reimbursements"/></form></th>
					<th><form action="reimbPen.do" method="post"><input type="submit" class="btnSmall" value="Pending Reimbursements"/></form></th>
					<th><form action="reimbApp.do" method="post"><input type="submit" class="btnSmall" value="Approved Reimbursements"/></form></th>
					<th><form action="reimbDen.do" method="post"><input type="submit" class="btnSmall" value="Denied Reimbursements"/></form></th>
					<th><form action="reimbRes.do" method="post"><input type="submit" class="btnSmall" value="Resolved Reimbursements"/></form></th>
				</tr>
			</table>
		</div><br/>
		<c:out value="${ reimbs }">Reimbs</c:out>
		<table class="table1">
		  <tr class="tr1">
		    <th class="endCap1 th1">Title</th>
		    <th class="th1">Type</th>
		    <th class="th1">Date Submitted</th>
		    <th class="th1">Date Resolved</th>
		    <th class="th1">Status</th>
		    <!-- <th class="th1">Attached File</th> -->
		    <th class="endCap2 th1"></th>
		  </tr>
		  <tr>
			  <c:choose>
			    <c:when test="${fn:length(reimbs) == 0}">
			  <td><p  style="colspan: 5;">Nothing to see here, Comrade.....move along!</p></td>
			  <%-- <td><c:out value="" /></td>
			  <td><c:out value="" /></td>
			  <td><c:out value="" /></td> --%>
			    </c:when>
			    <c:otherwise>
		  <c:forEach var="r" items="${reimbs}">
		    <tr class="tr1">
			  <td><c:out value="${r.name}" /></td>
			  <td><c:out value="${r.type}" /></td>
			  <td><c:out value="${r.dateSub}" /></td>
			  <td>
				  <c:choose>
				    <c:when test="${ r.statusID eq 0}">
			  			<c:out value="N/A"/>
				    </c:when>
				    <c:otherwise>
			  			<c:out value="${r.dateRes}"/>
				    </c:otherwise>
				  </c:choose>
			  </td>
			  <%-- <td><c:out value="${r.file}" /></td> --%>
			  	<c:if test="${ uRoleID eq 0}">
				  <c:choose>
				    <c:when test="${ r.statusID eq 0}">
				        <td>
				        	<form action="app.do" method="post">
				        		<input type="submit" style="width: 100%;"  value="Approve!"/>
				        	</form>
				        	<form action="den.do" method="post">
				        		<input type="submit" style="width: 100%;" value="Deny!"/>
				        	</form>
				    </c:when>
				    <c:otherwise>
				        <c:if test="${ r.statusID == 0 }"><p>Pending!</p></c:if>
				        <c:if test="${ r.statusID == 0 }"><p>Approved!</p></c:if>
				        <c:if test="${ r.statusID == 0 }"><p>Denied!</p></c:if>
				    </c:otherwise>
				  </c:choose>
 	 			<button type="button" id="btnCreate" class="btnSmall btn-info btn-lg" data-toggle="modal" data-target="#myModal">Create Reimbursement</button>
 	 			</c:if>
			  <td><a href="javascript:unhide('1');"><span class="glyphicon glyphicon-plus-sign"></span></a></td>
		    </tr>
		    <tr class="hidden reimbListHidden" id="1">
		       <td colspan="3"><c:out value="${r.details}"/></td>
		    </tr>
		  </c:forEach>
	    </c:otherwise>
	  </c:choose>
	  </tr>
		</table>
		<table class="reimbBottom">
			<tr>
				<%-- <th>				
				<fmt:parseNumber var="i" type="number" value="<%= session.getAttribute(\"uRoleID\") %>" />
				<c:if test="${ uRoleID eq 0}"><form action="approve.do" method="post"><input type="submit" class="btnSmall" value="Approve!"/></form></c:if>
				</th>
				<th>
				<c:if test="${ uRoleID eq 0}"><form action="deny.do" method="post"><input type="submit" class="btnSmall" value="Deny!"/></form></c:if>
				</th> --%>
				<th>
				<c:if test="${ uRoleID eq 1}">
 	 			<button type="button" id="btnCreate" class="btnSmall btn-info btn-lg" data-toggle="modal" data-target="#myModal">Create Reimbursement</button>
 	 			</c:if>
 	 			</th>
 	 		</tr>
 	 	</table>
	</div>
	
	<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reimbursement Creation</h4>
        </div>
        <div class="modal-body">
			<div id="creation">
			<form action="reimbNewSub.do" method="post">
				<p>Amount:</p><input type="number" class="inBoxes" name="amount" placeholder="Insert Amount!"/><br/><br/>
				<p>Description:</p><textarea rows="4" cols="20" class="inBoxes" name="descript" placeholder="Insert Description!"/></textarea><br/><br/>
				<p>Type:</p>
				<!-- <div  class="reimbRadios">
					<input type="radio" name="reimbType" onClick="javascript:radio()" checked>Food<br/>
					<input type="radio" name="reimbType" onClick="javascript:radio()">Travel<br/>
					<input type="radio" name="reimbType" onClick="javascript:radio()">Lodging<br/>
					<input type="radio" name="reimbType" onClick="javascript:radio()">Other<br/>
				</div> -->
					
					<select name="type">
						<option>Food</option>
						<option>Travel</option>
						<option>Lodging</option>
						<option>Other</option>
					</select>
				<br/><br/>

				<!-- <input type="file" id="upload" name="upload" accept=".jpg, .jpeg, .png"> style="visibility: hidden; width: 1px; height: 1px" />
				<input type="button" id="proof" value="Add Proof!" onclick="document.getElementById('upload').click(); return false"/>
				<br/><input type="text" name="path" id="path" readonly="readonly"/>
				<p name="filePath" id="path"></p>
				<input type="button" onclick="getPath()" value="Add File!" id="getPathBtn"/> -->
				<br/><br/>
				<input type="submit" id="sub" class="btnSmall" value="Submit!">
				</form>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btnSmall btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

</body>
</html>
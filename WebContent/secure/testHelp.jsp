<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<script type="text/javascript">
	  function unhide(divID) {
	    var item = document.getElementById(divID);
	    if (item) {
	      item.className=(item.className=='hidden')?'unhidden':'hidden';
	    }
	  }
	</script>
	
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
</head>
<body>
	<%@ include file="./navTest.jsp"%>
	
	<div class="rcorners1">
		<h1>Welcome to enlightenment, Comrade <c:out value="${role.id}"></c:out>!</h1>
		<br/><br/><br/><p>What wisdom do you seek from our glorious leader?</p>
	</div>
	
	<div id="faqsDiv">
    <a id="q1" href="javascript:unhide('reimbTypes');">What can I be reimbursed for?</a>
    <div id="col2">
	  <div id="reimbTypes" class="hidden">
    <br/>
	   <p>There are 4 types of reimbursement: Travel, Lodging, Food, and Other.</p>
	  </div>
	</div>
	
    <a id="q2" href="javascript:unhide('reimbHow');">How will I be reimbursed?</a>
    <div id="col2">
	  <div id="reimbHow" class="hidden">
    <br/>
	   <p>With money.</p>
	  </div>
	</div>
	
    <a id="q3" href="javascript:unhide('reimbWho');">Who can I contact about my reimbursement?</a>
    <div id="col2">
	  <div id="reimbWho" class="hidden">
    <br/>
	   <p>Send all emails to to Comrade Patrick at [patrick@revature.com].</p>
	  </div>
	</div>
	</div>
	
	<%@ include file="./footerTest.jsp"%>
</body>
</html>
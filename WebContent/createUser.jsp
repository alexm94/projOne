<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="cu.do" method="post">
		<input type="number" placeholder="UserID" name="userID"><br/>
		<input type="number" placeholder="UserRoleID" name="urid"><br/>
		<input type="text" placeholder="First Name" name="fname"><br/>
		<input type="text" placeholder="Last Name" name="lname"><br/>
		<input type="text" placeholder="Email" name="email"><br/>
		<input type="text" placeholder="Username" name="uname"><br/>
		<input type="password" placeholder="Password" name="upass"><br/>
		<input type="submit" value="Create User"/>
	</form>
	
	<c:if test="${not empty loginFail}"><c:out value="${loginFail}" /></c:if><br/>

</body>
</html>